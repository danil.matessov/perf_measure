import psutil
import subprocess
import wandb


wandb.login()
run = wandb.init(
    # Set the project where this run will be logged
    project="colmap",
    # A short display name for this run, which is how you'll identify this run in the UI
    name = "automatic_reconstructor-x0.1",
    # Track hyperparameters and run metadata
    config={
        "Num Images": 83,
        "Resolution": "x0.1",
        "Interval": 1
    },
)

# PROGRAM = ["python", "program.py"]
PROGRAM = ["colmap", "automatic_reconstructor", "--workspace_path", "/home/danil/COLMAPProjects/pyramid_x0.1/", "--image_path", "/home/danil/COLMAPProjects/pyramid_x0.1/images"]

process = subprocess.Popen(PROGRAM)
print(process.pid)

pr = psutil.Process(process.pid)

while True:
    try:
        process_name = pr.name()
        process_mem = pr.memory_percent()
        process_cpu = pr.cpu_percent(interval=1)
        if pr.status() == 'zombie':
            break
    except psutil.NoSuchProcess as e:
        print(e.pid, "killed before analysis")
    else:
        print("Name:", process_name)
        print("CPU%:", process_cpu)
        print("MEM%:", process_mem)

        wandb.log({"CPU%:": process_cpu, "MEM%:": process_mem})

process.kill()
